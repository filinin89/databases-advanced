package com.company.controller;

import com.company.dto.Category;
import com.company.dto.Product;
import com.company.service.ProductsService;
import com.company.service.impl.ProductsServiceImpl;
import com.company.manager.SessionUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class ProductsController {


    @Autowired
    private ProductsService productsService;

    @Autowired
    private SessionUserManager userManager;



    @PostMapping("/addCategory")
    public ModelAndView addCategoty(ModelAndView modelAndView, Category category, @RequestParam("name")String name){
        category.setName(name);
        productsService.addCategory(category); // добавим в базу
        modelAndView.addObject("ProductsCategory", productsService.getCategories()); // берем из базы, чтоб вывести во view
        modelAndView.setViewName("main_admin");
        return modelAndView;
    }

    @PostMapping("/addProduct")
    public ModelAndView addProduct(ModelAndView modelAndView, @ModelAttribute("addedProduct") Product addedProduct, HttpSession session){
        if(productsService.getProductById(addedProduct.getId()) != null){
            modelAndView.setViewName("redirect:/updateProduct"); // если такой продукт есть, то делаем обновление
            session.setAttribute("existingProduct", addedProduct);
            return modelAndView;
        }
        productsService.addProduct(addedProduct);
        modelAndView.setViewName("redirect:/main_admin");
        return modelAndView;
    }

    @GetMapping("/updateProduct")
    public String updateProduct(@RequestParam(required=false) Product updateProduct, ModelAndView modelAndView, HttpSession session){
        productsService.updateProduct((Product)session.getAttribute("existingProduct"));
        return "redirect:/main_admin";
    }


    // это можно к поиску по id отнести
    @GetMapping("/getProduct")
    public ModelAndView getProduct(@RequestParam Integer id, ModelAndView modelAndView){
        Product product = productsService.getProductById(id);
        modelAndView.setViewName("redirect:/main_admin");
        return modelAndView;
    }

    @PostMapping("/deleteProduct")
    public ModelAndView deleteProduct(@RequestParam(required=false) Integer id, ModelAndView modelAndView){
        if (productsService.getProductById(id) != null) {
            productsService.delete(productsService.getProductById(id));
        }
        modelAndView.setViewName("redirect:/main_admin");
        return modelAndView;
    }


    @PostMapping("/findByName")
    public ModelAndView findProductByName(@RequestParam("searchProduct") String searchProductName, ModelAndView modelAndView, HttpSession httpSession){
        List<Product> foundProducts = productsService.findProductByName(searchProductName);
        httpSession.setAttribute("foundProducts", foundProducts);
        modelAndView.setViewName("redirect:/main_admin");
        return modelAndView;
    }

    @PostMapping("/findByCost")
    public ModelAndView findProductByCost(@RequestParam("searchProduct") Integer searchProductCost, ModelAndView modelAndView, HttpSession httpSession){
        List<Product> foundProducts = productsService.findProductByCost(searchProductCost);
        httpSession.setAttribute("foundProducts", foundProducts);
        modelAndView.setViewName("redirect:/main");
        return modelAndView;
    }

    /*@GetMapping("/error")
    public ModelAndView errorOccurred(ModelAndView modelAndView){
        modelAndView.setViewName("error");
        return modelAndView;
    }*/
}
