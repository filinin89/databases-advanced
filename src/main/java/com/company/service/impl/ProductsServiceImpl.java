package com.company.service.impl;

import com.company.dao_spring_data.CategoryRepository;
import com.company.dao_spring_data.ProductRepository;
import com.company.dto.Category;
import com.company.dto.Product;
import com.company.repository.ProductsRepository;
import com.company.repository.impl.ProductsRepositoryImpl;
import com.company.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductsServiceImpl implements ProductsService {

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ProductRepository productRepository;




    //-----------
    //------ Hibernate
    //----------
    public void addProduct(Product product){
        productsRepository.addProduct(product);
    }

    public void addCategory(Category category){
        productsRepository.addCategory(category);
    }

    public void delete(Product product) {
        productsRepository.delete(product);
    }




    //-----------
    //------ Spring Data
    //----------
    public List<Category> getCategories(){
        return categoryRepository.findAll();
    }

    public Product getProductById(Integer id){
        if(productRepository.findById(id).isPresent()){
            return productRepository.findById(id).get();
        }else{
            return null;
        }
    }

    public void updateProduct(Product updateProduct) {
        productRepository.save(updateProduct);
    }

    public List<Product> findProductByName(String nameOfWantedProduct){
        return productRepository.findByName(nameOfWantedProduct);
    }

    public List<Product> findProductByCost(int costOfWantedProduct){
        return productRepository.findByCost(costOfWantedProduct);
    }





}
