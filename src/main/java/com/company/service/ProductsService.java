package com.company.service;

import com.company.dto.Category;
import com.company.dto.Product;


import java.util.List;

public interface ProductsService {


    public void addCategory(Category category);

    public List<Category> getCategories();

    public void addProduct(Product product);

    public Product getProductById(Integer id);

    public void updateProduct(Product updateProduct);

    public void delete(Product product);

    public List<Product> findProductByName(String wantedProductName);

    public List<Product> findProductByCost(int wantedProductCost);
}
