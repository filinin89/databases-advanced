package com.company.repository;

import com.company.dto.Category;
import com.company.dto.Product;

import java.util.ArrayList;

public interface ProductsRepository {


    public void addProduct(Product product);

    public boolean contains(Product product);

    public void addCategory(Category category);

    public void delete(Product product);

    public int getNUMBER_OF_SECTIONS();

    public void generateProducts();


}
