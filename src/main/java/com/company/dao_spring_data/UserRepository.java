package com.company.dao_spring_data;

import com.company.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer>{

    User findByLogin(String login);


}
