package com.company.dao_spring_data;

import com.company.dto.Order;
import com.company.dto.OrderedProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderedRepository extends JpaRepository<OrderedProduct, Integer> {


}
