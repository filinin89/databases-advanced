package com.company.dao_spring_data;

import com.company.dto.Order;
import com.company.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

public interface OrderRepository extends JpaRepository<Order, Integer> {


}
